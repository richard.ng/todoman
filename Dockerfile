FROM node:13.10.1-alpine

COPY package*.json app/

COPY test-helpers app/test-helpers
COPY src app/src

WORKDIR app

RUN npm install

EXPOSE 3000

CMD npm test
